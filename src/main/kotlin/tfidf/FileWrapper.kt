package tfidf

import java.io.File
import java.io.InputStream

class FileWrapper {
    fun read(filename: String): String {
        val inputStream: InputStream = File(filename).inputStream()
        return inputStream.bufferedReader().use { it.readText() }.toLowerCase().replace("\\p{Punct}"," ")
    }
}