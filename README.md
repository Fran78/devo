# README #



### Coste

* Palindrome: O(N/2) = O(N)

* KComplementary: O(N), caso peor O(N2)

* TfIdf: O(N), siendo N el número de documentos

### Ejecución

* En caso de KComplementary y Palindrome: No se ha hecho un main, se ejecutan mediante los tests: ./gradlew test

* En el caso del tfidf: 

  - Paso 1: ./gradlew build, desde el directorio raíz.
  
  - Paso 2: java -jar devo-1.0-SNAPSHOT-all.jar -n 5 -p 10 -d /tmp -t "state protection national", desde el directorio build/libs
 
 
 