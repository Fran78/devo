package tfidf

import org.apache.commons.math3.util.Precision
import kotlin.math.ln

interface Scorer {
    fun score(text: String, term: String): Double
}

class TfScorer(): Scorer {
    override fun score(text: String, term: String): Double = (text.split(term).size - 1).toDouble()


}

class TfIdfScorer(private val termMatch: HashMap<String, Int>, private val docsSize: Int): Scorer {
    private val scorer = TfScorer()
    //override fun score(text: String, term: String): Double = scorers.fold(1.0) { acc, scorer ->  acc * scorer.score(text, term)}
    override fun score(text: String, term: String): Double =  Precision.round(scorer.score(text, term) * idf(term), 4)
    private fun idf(term: String): Double {
        val teD = termMatch[term] ?: 1
        return ln(docsSize.toDouble() / teD.toDouble())
    }
}