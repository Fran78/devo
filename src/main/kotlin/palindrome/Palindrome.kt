package palindrome


//La complejidad es de O(N/2) = O(N)
class Palindrome {
    fun isPalindrome(text: String): Boolean {
        if (text.isEmpty()) return true //El string vacío y el string de un solo carácter lo considero Palíndromo.
        val stringLength = text.length
        for (i in 0 .. stringLength / 2) {
            if (text[i] != text[stringLength - i - 1]) return false
        }
        return true
    }
}