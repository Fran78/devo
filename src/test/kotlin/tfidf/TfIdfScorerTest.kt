package tfidf

import org.testng.Assert
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

class TfIdfScorerTest {
    @DataProvider(name = "tfIdfScorerData")
    fun createData(): Array<Array<Any>>? {
        return arrayOf(arrayOf("en un lugar de la mancha de cuyo nombre", "de", 2.7726),
                arrayOf("no traje traje y me compré otro traje", "traje", 2.0794),
                arrayOf("la vita e bella", "traje", 0.0))
    }

    @Test(dataProvider = "tfIdfScorerData")
    fun test_score_returnCorrectResult(text: String, term: String, expected: Double) {
        val docsSize = 8
        val termMatch = hashMapOf("traje" to 4, "de" to 2)
        val tfIdfScorer = TfIdfScorer(termMatch, docsSize)
        val actual = tfIdfScorer.score(text, term)
        Assert.assertEquals(expected, actual)
    }
}