package tfidf

import java.util.*

data class DocumentTfIdf(val documentName: String, val score: Double) {
    override fun toString(): String = "$documentName $score"
}


class DocumentsHandler(private val topN: Int, private val terms: String) {

    fun processDocuments(documents: List<String>): List<String>  {
        val termMatch = hashMapOf<String, Int>()
        val result: HashMap<String, Double> = hashMapOf()
        documents.forEach {
            val document = Document(it, TfScorer(), FileWrapper())
            document.match(terms, termMatch)
        }
        documents.forEach {
            val document = Document(it, TfIdfScorer(termMatch, documents.size), FileWrapper())
            val score = document.score(terms)
            result[score.documentName] = score.score
        }
        return result.entries.sortedWith(kotlin.Comparator {
                o1, o2 -> if (o1.value >= o2.value) -1 else 1
        }).map { it.key + " " + it.value }.subList(0, topN)
    }
}