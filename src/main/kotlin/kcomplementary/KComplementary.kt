package kcomplementary



//El coste temporal es O(N). En el caso peor sería O(N2) debido al filter que hay en el segundo bucle.
// Si no hubiera repetidos, el coste sería O(N)
class KComplementary {


    fun findKComplementaries(k: Int, nums: Array<Int>): Set<Pair<Int, Int>> {
        val difference = hashMapOf<Int, MutableList<Int>>()
        val length = nums.size
        val result = mutableSetOf<Pair<Int, Int>>()
        for (i in 0 until length) {
            val index = k - nums[i]
            if (difference[index] != null) difference[index]!!.add(i)
            else difference[index] = mutableListOf(i)
        }

        for (j in 0 until length) {
            val exist = difference[nums[j]]
            if (exist != null) result.addAll(exist.filter { it != j }.map { Pair(it, j) })
        }
        return result
    }
}