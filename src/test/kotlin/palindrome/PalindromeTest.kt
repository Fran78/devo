package palindrome

import org.testng.Assert
import org.testng.annotations.DataProvider
import org.testng.annotations.Test


class PalindromeTest {


    @DataProvider(name = "palindromeData")
    fun createData(): Array<Array<Any>>? {
        return arrayOf(arrayOf("astsa", true),
            arrayOf("asttsa", true), arrayOf("astyutsa", false),
            arrayOf("2", true), arrayOf("", true))
    }


    @Test(dataProvider = "palindromeData")
    fun test_isPalindrome_returnCorrectResult(text: String, expected: Boolean) {
        val actual = Palindrome().isPalindrome(text)
        Assert.assertEquals(expected, actual)
    }

}