package tfidf


import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import org.testng.Assert

class DocumentTest {
    @DataProvider(name = "documentData")
    fun createData(): Array<Array<Any>>? {
        return arrayOf(arrayOf("en un lugar de la mancha de cuyo nombre", "de mancha", 2, 1),
                arrayOf("no traje traje y me compré otro traje", "traje bello", 1, 0),
                arrayOf("la vita e bella", "traje gris", 0, 3))
    }


    @Test(dataProvider = "documentData")
    fun test_match_returnCorrectResult(text: String, terms: String, expected1: Int, expected2: Int) {
        val sut = setupDocument(TfScorer(), text)
        val termMatch = hashMapOf("de" to 1, "gris" to 3)
        sut.match(terms, termMatch)
        val strTerms = terms.split(" ")
        val actual1 = termMatch[strTerms[0]]?:0
        val actual2 = termMatch[strTerms[1]]?:0
        val expected = "$expected1,$expected2"
        val actual = "$actual1,$actual2"
        Assert.assertEquals(actual, expected)
    }




    @Test
    fun test_score_calledWithSeveralTerms_returnSumScorerAllTerms() {
        val text = "a dummy text for test purposes"
        val terms = "dummy test a"
        val scorerStub = mock<Scorer> {
            on { score(text, "dummy")} doReturn 3.4
            on { score(text, "test")} doReturn 2.0
            on { score(text, "a")} doReturn 1.7
        }
        val sut = setupDocument(scorerStub, text)
        val actual = sut.score(terms)
        val expected = "dummy.txt 7.1"
        Assert.assertEquals(actual.toString(), expected)
    }


    private fun setupDocument(scorer: Scorer, text: String): Document {
        val filename = "dummy.txt"
        val fileWrapperStub = mock<FileWrapper> {
            on { read(filename) } doReturn text
        }
        return Document(filename, scorer, fileWrapperStub)
    }
}