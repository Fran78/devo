import org.apache.commons.cli.*
import org.slf4j.LoggerFactory
import tfidf.DocumentsHandler
import java.io.File
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit



//El coste temporal es O(N), siendo N el número de documentos.
class Application {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val parser: CommandLineParser = DefaultParser()


            val options = Options()
            options.addOption(Option.builder("n")
                    .required(true).hasArg(true).type(Int::class.java)
                    .desc("the count N of top results to show")
                    .build())

            options.addOption(Option.builder("d")
                    .required(true).hasArg(true).type(String::class.java)
                    .desc("the directory D where the documents will be written")
                    .build())

            options.addOption(Option.builder("t")
                    .required(true).hasArg(true).type(String::class.java)
                    .desc("the terms TT to be analyzed")
                    .build())

            options.addOption(Option.builder("p")
                    .required(true).hasArg(true).type(Long::class.java)
                    .desc("the period P in seconds to report the top N\"")
                    .build())


            val line: CommandLine = parser.parse(options, args)
            val topN = line.getOptionValue("n")
            val directory = line.getOptionValue("d")
            val tt = line.getOptionValue("t")
            val period = line.getOptionValue("p").toLong()

            var count = 0
            val ses = Executors.newScheduledThreadPool(1)
            val documentsHandler = DocumentsHandler(topN.toInt(), tt)
            val task1 = Runnable {
                count++
                println("Running...task1 - count : $count")
                val documents = File(directory).listFiles().toList()
                val result = documentsHandler.processDocuments(documents.filter { it.absolutePath.endsWith(".txt") }.map { it.absolutePath })
                result.forEach { println(it) }

            }
            ses.scheduleAtFixedRate(task1, 5, period, TimeUnit.SECONDS)
        }

    }
}