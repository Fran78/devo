package kcomplementary

import org.testng.Assert
import org.testng.annotations.DataProvider
import org.testng.annotations.Test


class KComplementaryTest {
    @DataProvider(name = "kcomplementaryData")
    fun createData(): Array<Array<Any>>? {
        return arrayOf(arrayOf(7, arrayOf(4, 5, 1, 3, 5, 1, 8, -7, -6), mutableListOf(Pair(3, 0), Pair(0, 3))),
            arrayOf(6, arrayOf(4, 5, 1, 3, 5, 1, 8, -7, -6), mutableListOf(Pair(2, 1), Pair(5, 1),Pair(1, 2), Pair(4, 2),Pair(2, 4),
                Pair(5, 4),Pair(1, 5), Pair(4, 5))))
    }


    @Test(dataProvider = "kcomplementaryData")
    fun test_findKComplementaries_returnCorrectResult(k: Int, nums: Array<Int>, expected: MutableList<Pair<Int, Int>>) {
        val actual = KComplementary().findKComplementaries(k, nums)
        Assert.assertEquals(expected, actual)
    }
}