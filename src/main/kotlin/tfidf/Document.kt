package tfidf

import org.apache.commons.math3.util.Precision
import java.io.File
import java.io.InputStream

class Document(private val filename: String, private val scorer: Scorer, private val fileWrapper: FileWrapper) {

    private val inputString: String = fileWrapper.read(filename)

    fun match(terms: String, termMatch: HashMap<String, Int>) {
        terms.split(" ").forEach {
            if (inputString.contains(it)) {
                termMatch[it] = (termMatch[it]?.plus(1))?:1
            }
        }
    }

    fun score(terms: String): DocumentTfIdf = DocumentTfIdf(filename, Precision.round(terms.split(" ").fold(0.0) { acc, t -> acc + scorer.score(inputString, t) }, 4))
}