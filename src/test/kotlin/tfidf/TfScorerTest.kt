package tfidf

import org.testng.Assert
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

class TfScorerTest {
    @DataProvider(name = "tfScorerData")
    fun createData(): Array<Array<Any>>? {
        return arrayOf(arrayOf("en un lugar de la mancha de cuyo nombre", "de", 2.0),
                arrayOf("no traje traje y me compré otro traje", "traje", 3.0),
                arrayOf("la vita e bella", "traje", 0.0))
    }

    @Test(dataProvider = "tfScorerData")
    fun test_score_returnCorrectResult(text: String, term: String, expected: Double) {
        val tfScorer = TfScorer()
        val actual = tfScorer.score(text, term)
        Assert.assertEquals(actual, expected)
    }
}